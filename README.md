Utiliser XPath
==============

En utilisant le module PHP DOM (https://www.php.net/manual/fr/book.dom.php)

# Etape 1 
https://www.php.net/manual/fr/domdocument.loadxml.php
Charger le document myTodo.xml avec DOM::Document

# Etape 2
https://www.php.net/manual/fr/domxpath.query.php
Faites une requête XPath avec DOMXPath, lister toutes les balises subtask

# Etape 3
Charger toutes les subtask avec un statut égal à "todo"

# Etape 4
Charger tous les items avec au moins une subtask qui n'est pas optionnelle et qui
a un statut égal à "todo"

